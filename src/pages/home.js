import React, { useState } from "react";
import Pic from "../ss.png";
export default function Home() {
  const [heightCSS, setHeightCss] = useState("h2");
  const [ste, setSte] = useState(0);
  const [ste1, setSte1] = useState(0);
  function changeBackground(e) {
    // e.target.style.background = "#2E2F33";
    // e.target.style.height = "10rem";
    setHeightCss("h5");
    setSte(ste + 1);
  }

  function changeBackground2(e) {
    // e.target.style.background = "#2E2F33";
    // e.target.style.height = "2rem";
    setHeightCss("h2");
    setSte(ste - 1);
  }

  const productDetails = [
    {
      name: "Airpods Pro",
      price: 24900,
      imageUrl:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTJiKtlpQGkIeOyAPV3qQMNkl8uuRzfGWZtIDb_WgDnam8WjhpL&usqp=CAU",
      qty: 10,
      heading: "Wireless Noise Cancelling Earphones",
      des: "AirPods Pro have been designed to deliver active Noise Cancellation for immersive sound. Transparancy mode so much can hear your surroundings.",
    },

    {
      name: "Apple Watch",
      price: 40900,
      imageUrl: "https://purepng.com/public/uploads/large/apple-watch-pcq.png",
      qty: 15,
      heading: "You’ve never seen a watch like this",
      des: "The most advanced Apple Watch yet, featuring the Always-On Retina display, the ECG app, international emergency calling, fall detection and a built‑in compass.",
    },

    {
      name: "Macbook Pro",
      price: 199900,
      imageUrl: "https://pngimg.com/uploads/macbook/macbook_PNG8.png",
      qty: 20,
      heading: "The best for the brightest",
      des: "Designed for those who defy limits and change the world, the new MacBook Pro is by far the most powerful notebook we’ve ever made. it’s the ultimate pro notebook for the ultimate user.",
    },

    {
      name: "iPhone 11 pro",
      price: 106600,
      imageUrl:
        "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-11-pro-midnight-green-select-2019?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1566954990073",
      qty: 35,
      heading: "Pro cameras. Pro display. Pro performance",
      des: "A mind‑blowing chip that doubles down on machine learning and pushes the boundaries of what a smartphone can do. Welcome to the first iPhone powerful enough to be called Pro.",
    },

    {
      name: "iPad Pro",
      price: 71900,
      imageUrl:
        "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/ipad-pro-12-select-wifi-spacegray-202003_FMT_WHH?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1583553704156",
      qty: 25,
      heading: "Your next computer is not a computer",
      des: "It’s a magical piece of glass. It’s so fast most PC laptops can’t catch up. And you can use it with touch, pencil, keyboard and now trackpad. It’s the new iPad Pro.",
    },
  ];

  return (
    <div>
      <div class="bubbles">
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
      </div>

      <div className="flex w-100 min-vh-80  items-center justify-center ">
        {productDetails.map((item, index) => {
          return (
            <div className="w-20 h7 mh2 br4 pointer tc  sss relative">
              <div className=" fw6">
                {" "}
                <img  src={'https://gift.virtuallive.in/wp-content/uploads/2020/12/shubharambh-trophies31b.png'} alt="" />
                {"Product Name"}

                <div  className="flex flex-column w-100 items-center justify-center fw6 mt2" >
                    <div>  <button className="mh2" onClick={()=>setSte1(ste1+1)}>+</button>
                    {ste1}
                    <button className="mh2" onClick={()=>setSte1(ste1-1)}>-</button></div>
                  
<div className="mv2">Add to cart</div>

                </div>
              </div>

              <div
                onMouseOver={changeBackground}
                onMouseOut={changeBackground2}
                className={` absolute bottom-0 ${heightCSS} mt3 flex flex-column items-center justify-end  z-max f3 fw6 br4 w-100 white `}
                style={{ background: "#2E2F33" }}
              >
                {ste == 1 && (
                  <p className="f5">lorem ipsum dolor sit amet, consectetur adip
                      lorem ipsum dolor sit amet, consectetur adip
                      lorem ipsum dolor sit amet, consectetur adip
                     
                  </p>
                )}
               

               
              <span className="f4 flex  items-center justify-center "> View More</span> 
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
